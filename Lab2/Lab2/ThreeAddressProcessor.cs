﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class ThreeAddressProcessor : Processor
    {
        private int _registerQuantity;

        public override string StatusString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < Registers.Count; i++)
                {
                    sb.AppendLine(string.Format("Регистр №{0} = {1}", i, Registers[i]));
                }
                sb.AppendLine(string.Format("Номер команды = {0}", CommandNumber));
                sb.AppendLine(string.Format("Номер такта = {0}", TactNumber));
                sb.AppendLine(string.Format("Статус процессора = {0}", LastCommandResultStatus));
                return sb.ToString();
            }
        }

        public ThreeAddressProcessor(int bitsQunatity, int registersQunatity) : base(bitsQunatity)
        {
            if (registersQunatity > 1)
                _registerQuantity = registersQunatity;
            else
                throw new Exception(
                    "You can create objects of this class only with registers quantity with more than 1 register."
                    );
            AddressType = ProcessorAddressType.ThreeAddress;
            Registers.AddRange(Enumerable.Repeat<string>(ZeroString, _registerQuantity));
        }

        public override void Load(int value, int registerNumber = 0)
        {
            base.Load(value, registerNumber);
            Registers[registerNumber] = DecimalToComplementaryCode(value);
            TactNumber = 2;
        }

        public override void Add(int register1, int register2)
        {
            base.Add(register1, register2);
            string result = AddBinary(Registers[register1], Registers[register2]);
            LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
            Registers[register1] = result;
            TactNumber = 2;
        }

        public override void Add(int register1, int register2, int register3)
        {
            base.Add(register1, register2, register3);
            string result = AddBinary(Registers[register2], Registers[register3]);
            Registers[register2] = result;
            TactNumber = 2;
            result = AddBinary(Registers[register1], Registers[register2]);
            Registers[register1] = result;
            TactNumber = 3;
            LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
        }

        public override void ShiftLeft(int registerNumber)
        {
            base.ShiftLeft(registerNumber);
            string result = LeftShift(Registers[registerNumber]);
            LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
            Registers[registerNumber] = result;
            TactNumber = 2;
        }

        public override void ShiftLeft(int register1, int register2)
        {
            base.ShiftLeft(register1, register2);
            string result = LeftShift(Registers[register1]);
            Registers[register1] = result;
            TactNumber = 2;
            result = LeftShift(Registers[register2]);
            Registers[register2] = result;
            TactNumber = 3;
            LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
        }

        public override void ShiftLeft(int register1, int register2, int register3)
        {
            base.ShiftLeft(register1, register2, register3);
            string result = LeftShift(Registers[register1]);
            Registers[register1] = result;
            TactNumber = 2;
            result = LeftShift(Registers[register2]);
            Registers[register2] = result;
            TactNumber = 3;
            result = LeftShift(Registers[register3]);
            Registers[register3] = result;
            TactNumber = 4;
            LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
        }

        public override void ShiftRight(int registerNumber)
        {
            base.ShiftRight(registerNumber);
            string result = RightShift(Registers[registerNumber]);
            LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
            Registers[registerNumber] = result;
            TactNumber = 2;
        }

        public override void ShiftRight(int register1, int register2)
        {
            base.ShiftLeft(register1, register2);
            string result = RightShift(Registers[register1]);
            Registers[register1] = result;
            TactNumber = 2;
            result = RightShift(Registers[register2]);
            Registers[register2] = result;
            TactNumber = 3;
            LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
        }

        public override void ShiftRight(int register1, int register2, int register3)
        {
            base.ShiftLeft(register1, register2, register3);
            string result = RightShift(Registers[register1]);
            Registers[register1] = result;
            TactNumber = 2;
            result = RightShift(Registers[register2]);
            Registers[register2] = result;
            TactNumber = 3;
            result = RightShift(Registers[register3]);
            Registers[register3] = result;
            TactNumber = 4;
            LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
        }
    }
}
