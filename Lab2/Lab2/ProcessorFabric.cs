﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public static class ProcessorFabric
    {
        public static Processor CreateProcessor(int bitsQuantity, ProcessorAddressType processorAddressType, int registersQuantity = 2)
        {
            if (processorAddressType == ProcessorAddressType.OneAddress)
                return new OneAddressProcessor(bitsQuantity);
            else if (processorAddressType == ProcessorAddressType.ThreeAddress)
                return new ThreeAddressProcessor(bitsQuantity, registersQuantity);
            else if (processorAddressType == ProcessorAddressType.StackAddress)
                return new StackAddressProcessor(bitsQuantity);
            else
                throw new Exception(
                    "You can't create a processor with such configuration."
                    );
        }
    }
}
