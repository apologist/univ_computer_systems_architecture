﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;


namespace Lab2
{
    public enum ProcessorAddressType
    {
        None = 0,
        OneAddress = 1,
        TwoAddress = 3,
        ThreeAddress = 2,
        StackAddress = 4
    }
    
    class Program
    {
        private static void Debug(string message)
        {
            if (Processor.Debug)
                Console.WriteLine(message);
        }

        public static Processor Processor { get; set; }

        static void Main(string[] args)
        {
            int n, bitsQuantity, registersQuantity;
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            string bitsQuantityString = string.Empty;
            while (!int.TryParse(bitsQuantityString, out n))
            {
                Console.Write("Битность : ");
                bitsQuantityString = Console.ReadLine();
            }
            bitsQuantity = n;

            string registersQuantityString = string.Empty;
            while (!int.TryParse(registersQuantityString, out n))
            {
                Console.Write("Количество регистров : ");
                registersQuantityString = Console.ReadLine();
            }
            registersQuantity = n;

            Processor = ProcessorFabric.CreateProcessor(bitsQuantity, ProcessorAddressType.ThreeAddress, registersQuantity);
            Processor.Debug = true;
            Console.WriteLine(Processor.StatusString);


            string input = string.Empty;
            while(true)
            {
                input = Console.ReadLine();
                switch(input.Trim())
                {
                    default:
                        try
                        {
                            Processor.ExecuteCommand(input);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception occured: {0}", ex.Message);
                        }
                    break;
                }
            }
            
        }
    }
}
