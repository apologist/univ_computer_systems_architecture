﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class OneAddressProcessor : Processor
    {
        private string Accumulator { get; set; }

        private string Register
        {
            get { return Registers[0]; }
            set { Registers[0] = value; }
        }

        public override string StatusString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(string.Format("Accumulator = {0}", Accumulator));
                sb.AppendLine(string.Format("Register = {0}", Register));
                sb.AppendLine(string.Format("Command number = {0}", CommandNumber));
                sb.AppendLine(string.Format("Tact number = {0}", TactNumber));
                sb.AppendLine(string.Format("Processor status = {0}", LastCommandResultStatus));
                return sb.ToString();
            }
        }

        public OneAddressProcessor(int bitsQunatity) : base(bitsQunatity)
        {
            AddressType = ProcessorAddressType.OneAddress;
            Registers.Add(ZeroString);
        }

        public override void Load(int n, int registerNumber = 1)
        {
            base.Load(n, registerNumber);
            Accumulator = DecimalToComplementaryCode(n);
            TactNumber = 2;
        }

        public override void Add(int register1, int register2)
        {
            base.Add(register1, register2);
            //string result = AddBinary(Registers[register1 + 1], Registers[register2 + 2]);
            //LastCommandResultStatus = (result[0] == '0') ? 0 : 1;
            //Register = result;
            //TactNumber = 2;
        }
    }
}
