﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Lab2
{
    public abstract class Processor
    {
        #region Constructors

        public Processor(int bitsQunatity)
        {
            if (bitsQunatity > 1)
                _bitsQunatity = bitsQunatity;
            else
                throw new Exception(
                    "You can define processor only with at least 2 bits."
                    );
            AddressType = ProcessorAddressType.None;
            Registers = new List<string>();
            Operands = new List<int>();
        }

        #endregion

        #region Variables

        private int _bitsQunatity;
        private ProcessorAddressType _addressType;
        private int _commandNumber;
        private int _tactNumber;

        #endregion

        #region Properties

        public int CommandNumber
        {
            get { return _commandNumber; }
        }

        public int TactNumber
        {
            get { return _tactNumber; }
            set
            {
                _tactNumber = value;
                if (Debug)
                    StatusConsoleDebug();
            }
        }

        public bool  Debug { get; set; }

        public virtual string StatusString
        {
            get
            {
                return "";
            }
        }

        public virtual string HelpString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("To finish simulation type \"quit\" or \"exit\" or \"q\".");
                var dict = CommandsDictionary[_addressType];
                sb.AppendLine("To get help about command type \"help COMMAND\".");
                sb.AppendLine("Next commands are available:");
                foreach (var key in dict.Keys.ToList())
                    sb.AppendLine(key);
                return sb.ToString();
            }
        }

        protected List<string> Registers { get; set; }

        protected List<int> Operands { get; set; }

        public int LastCommandResultStatus { get; set; }

        protected string ZeroString
        {
            get { return string.Concat(Enumerable.Repeat("0", _bitsQunatity)); }
        }

        protected string OneString
        {
            get { return string.Concat(Enumerable.Repeat("0", _bitsQunatity - 1)) + "1"; }
        }

        protected ProcessorAddressType AddressType
        {
            get { return _addressType; }
            set { _addressType = value; }
        }

        

        #endregion

        private void StatusConsoleDebug()
        {
            Console.WriteLine(StatusString);
        }

        private void IncreaseCommand()
        {
            _commandNumber++;
            Operands = new List<int>();
        }

        public virtual void Load(int n, int registerNumber = 0)
        {
            IncreaseCommand();
            TactNumber = 1;
            LastCommandResultStatus = (n < 0) ? 1 : 0;
        }

        public virtual void Add(int register1, int register2)
        {
            IncreaseCommand();
            TactNumber = 1;
        }

        public virtual void Add(int register1, int register2, int register3)
        {
            IncreaseCommand();
            TactNumber = 1;
        }

        public virtual void ShiftRight(int registerNumber)
        {
            IncreaseCommand();
            TactNumber = 1;
        }

        public virtual void ShiftRight(int register1, int register2)
        {
            IncreaseCommand();
            TactNumber = 1;
        }

        public virtual void ShiftRight(int register1, int register2, int register3)
        {
            IncreaseCommand();
            TactNumber = 1;
        }

        public virtual void ShiftLeft(int registerNumber)
        {
            IncreaseCommand();
            TactNumber = 1;
        }

        public virtual void ShiftLeft(int register1, int register2)
        {
            IncreaseCommand();
            TactNumber = 1;
        }

        public virtual void ShiftLeft(int register1, int register2, int register3)
        {
            IncreaseCommand();
            TactNumber = 1;
        }

        protected string AddBinary(string s1, string s2)
        {
            int n1 = Convert.ToInt32(s1, 2);
            int n2 = Convert.ToInt32(s2, 2);
            string s = Convert.ToString(n1 + n2, 2);
            if (s.Length > _bitsQunatity)
                throw new StackOverflowException();
            while (s.Length < _bitsQunatity)
                s = ((n1 + n2 > 0) ? "0" : "1") + s;
            return s;
        }

        public virtual void ExecuteCommand(string commandInput)
        {
            ProcessCommand(commandInput);
        }

        public string CommandHelp(string commandName)
        {
            var dict = CommandsDictionary[_addressType];
            if (dict.ContainsKey(commandName.ToLowerInvariant()))
            {
                string command = commandName.ToLowerInvariant();
                string help = dict[command];
                return string.Format("{0} : {1}", command, help);
            }
            else return string.Empty;
                
        }

        protected void ProcessCommand(string command)
        {
            if (string.IsNullOrWhiteSpace(command))
                throw new Exception(
                    "Command is empty!"
                    );
            var splittedCommandString = command.Split().ToList();
            int operandsCount = splittedCommandString.Count - 1; //first element is command name
            string commandName = splittedCommandString.First();

            int n; // temp
            if(int.TryParse(command, out n))
                throw new Exception(
                    "You should specify a name of the command."
                    );

            foreach(var operand in splittedCommandString.GetRange(1, splittedCommandString.Count - 1))
            {
                if (!int.TryParse(operand, out n))
                    throw new Exception(
                        "All operands should have a numeric value."
                        );
                Operands.Add(n);
            }

            ConsoleWrite(string.Format("Команда = {0} операнды: {1}\n", commandName, JsonConvert.SerializeObject(Operands)));

            switch (commandName.ToLowerInvariant())
            {
                case "load":
                    if (Operands.Count == 1) Load(Operands[0]);
                    if (Operands.Count == 2) Load(Operands[0], Operands[1]);
                    break;
                case "add":
                    if (Operands.Count == 2) Add(Operands[0], Operands[1]);
                    if (Operands.Count == 3) Add(Operands[0], Operands[1], Operands[2]);
                    break;
                case "shiftright":
                    if (Operands.Count == 1) ShiftRight(Operands[0]);
                    if (Operands.Count == 2) ShiftRight(Operands[0], Operands[1]);
                    if (Operands.Count == 3) ShiftRight(Operands[0], Operands[1], Operands[2]);
                    break;
                case "shiftleft":
                    if (Operands.Count == 1) ShiftLeft(Operands[0]);
                    if (Operands.Count == 2) ShiftLeft(Operands[0], Operands[1]);
                    if (Operands.Count == 3) ShiftLeft(Operands[0], Operands[1], Operands[2]);
                    break;
                default:
                    ConsoleWrite("There is no such instruction: " + commandName);
                    break;
            }
        }

        protected string LeftShift(string value, int shift = 1)
        {
            shift %= value.Length;
            return value.Substring(shift) + value.Substring(0, shift);
        }

        protected string RightShift(string value, int shift = 1)
        {
            shift %= value.Length;
            return value.Substring(value.Length - shift) + value.Substring(0, value.Length - shift);
        }

        protected string DecimalToComplementaryCode(int n)
        {
            if (Math.Pow(2, _bitsQunatity - 1) < n)
                throw new Exception(
                        string.Format(
                            "A number you want to operate on is too big for this processor. Max value : {0}.", 
                            Math.Pow(2, _bitsQunatity - 1).ToString()
                        ));
            string binaryString = Convert.ToString(n, 2);
            while (binaryString.Length < _bitsQunatity)
                binaryString = "0" + binaryString;
            while (binaryString.Length > _bitsQunatity)
                binaryString = binaryString.Substring(1);
            //if (n < 0) binaryString = Inverse(binaryString);
            return binaryString;
        }

        private string Inverse(string input)
        {
            StringBuilder sb = new StringBuilder(input);
            for(int i = 0; i < sb.Length; i++)
            {
                sb[i] = (input[i] == '0') ? '1' : '0';
            }
            return sb.ToString();
        }

        protected void ConsoleWrite(string message)
        {
            if (Debug)
                Console.WriteLine(message);
        }

        // help dictionary
        Dictionary<ProcessorAddressType, Dictionary<string, string>> CommandsDictionary
        {
            get
            {
                return new Dictionary<ProcessorAddressType, Dictionary<string, string>>()
                {
                    { ProcessorAddressType.ThreeAddress, new Dictionary<string, string>
                    {
                        {"load", "Loads a value to processor register.\n Load(int value, int registerNumber = 0)\n First operand is obligatorily, second one isn't." },
                        {"add", "Adds values in registers.\n Add(int register1, int register2)\n Add(int register1, int register2, int register3)\n All operands are obligatory." },
                        {"shiftright", "Shift values in register(s).\n ShiftRight(int registerNumber)\n ShiftRight(int register1, int register2)\n ShiftRight(int register1, int register2, int register3)\n All operands are obligatory." },
                        {"shiftleft", "Shift values in register(s).\n ShiftLeft(int registerNumber)\n ShiftLeft(int register1, int register2)\n ShiftLeft(int register1, int register2, int register3)\n All operands are obligatory." },
                    }},
                };
            }
        }

    }
}
