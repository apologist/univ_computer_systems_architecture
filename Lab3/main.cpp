	#include <bits/stdc++.h>
	#include <curses.h>
	const long double eps = 1e-9;
	const int MANT = 10, EXP = 11, BITS = 23;
	using namespace std;
	vector<int> minNotZero,
				maxDouble, 
				minNegDouble, 
				plusInf, 
				minusInf, 
				NotANum, 
				num1e0, 
				anynum, 
				num0e0;
				
	vector<int> cur;

	class Stand754{
		
	private:
		string convertCharacteristic(int x){
			string ret = "";
			while(x){
				ret += (x % 2) + '0';
				x /= 2;
			}
			while(ret.size() < EXP) 
				ret += '0';
			reverse(ret.begin(), ret.end());
			return ret;
		}
		
		string convertMantissa(double f){
			string ret = "1";
			string tmp = "";
			f -= 1.0;
			for(int i = 0; i < MANT; ++i){
				double cur = f * 2.0;
				if (cur >= 1.0){
					tmp += '1';
					cur -= 1.0;
				}
				else tmp += '0';
				f = cur;
			}
			cout << endl;
			reverse(tmp.begin(), tmp.end());
			ret += tmp;
			return ret;
		}
		
		pair<long double, int> toStand754(long double f){
			int p = 0;
			if (f >= 2.0){
				while(f >= 2.0){
					f /= 2.0;
					p++;
				}
			}
			else {
				while(f < 1.0){
					f *= 2.0;
					p--;
				}
			}
			if (fabs(f - 2.0) < eps){
				f /= 2.0;
				p++;
			}
			return make_pair(f, p);
		}
		
	public:
		Stand754(){
			minNotZero.push_back(0);
			for(int i = 0; i < EXP; ++i) 
				minNotZero.push_back(0);
				minNotZero.push_back(1);
			for(int i = 0; i < MANT - 2; ++i) 
				minNotZero.push_back(0);
			minNotZero.push_back(1);
			double f = 2147483647.0;
			trans(f, maxDouble);
			f = -2147483648.0;
			trans(f, minNegDouble);
			f = 754.0;
			trans(f, anynum);
			f = 1.0;
			trans(f, num1e0);
			plusInf.push_back(0);
			for(int i = 0; i < MANT; ++i) 
				plusInf.push_back(1);
			for(int i = 0; i < EXP; ++i) 
				plusInf.push_back(0);
			minusInf.push_back(1);
			for(int i = 0; i < MANT; ++i) 
				minusInf.push_back(1);
			for(int i = 0; i < EXP; ++i) 
				minusInf.push_back(0);
			NotANum.push_back(0);
			for(int i = 0; i < MANT; ++i) 
				NotANum.push_back(1);
			for(int i = 0; i < EXP -1; ++i) 
				NotANum.push_back(0);
			NotANum.push_back(1);
			num0e0.push_back(0);
			for(int i = 0; i < MANT; ++i) 
				num0e0.push_back(0);
			for(int i = 0; i < EXP; ++i) 
				num0e0.push_back(0);
		}
		
		void trans(double f, vector<int>& cur){
			int sign = 0;
			if (f < 0.0) sign = 1;
			f = fabs(f);
			pair<long double, int> num = toStand754(f);
			string charac = convertCharacteristic(num.second + 1023);
			string mant = convertMantissa(num.first);
			cur.clear();
			cur.push_back(sign);
			for(int i = 0; i < EXP; ++i) 
				cur.push_back(charac[i] - '0');
			for(int i = 0; i <= MANT; ++i) 
				cur.push_back(mant[i] - '0');
		}
		
		void outStand(){
			cout << "Minimal Number That is Not Zero in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << minNotZero[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
			cout << "The Biggest Double in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << maxDouble[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
			cout << "The Smallest Double in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << minNegDouble[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
			cout << "Plus Infinity in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << plusInf[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
			cout << "Minus Infinity in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << minusInf[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
			cout << "NAN in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << NotANum[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
			cout << "1.0 in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << num1e0[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
			cout << "0.0 in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << num0e0[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
			cout << "754 in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << anynum[i] << (i == 0? "  ": (i == MANT? "  ": " "));
			}
			cout << "\n\n";
		}
		void outCur(){
			cout << "Current Number in IEEE754 :\n";
			for(int i = 0; i < BITS; ++i){
				cout << cur[i] << (i == 0? "  ": (i == EXP ? "  ": " "));
			}
			cout << "\n\n";
		}
	} stand;

	int main(){
		stand.outStand();
		long double num;
		while(cin >> num){
			cur.clear();
			if (num == 0.0){
				cout << "0.0 in IEEE754 :\n";
				for(int i = 0; i < BITS; ++i){
					cout << num0e0[i] << (i == 0? "  ": (i == MANT? "  ": " "));
				}
				cout << "\n\n";
				continue;
			}
			stand.trans(num, cur);
			stand.outCur();
		}
		return 0;
	}
