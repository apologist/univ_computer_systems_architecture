#include <iostream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <string>
#include <vector>
#include <typeinfo>
#include "SuperFactory.h"
using namespace std;

#pragma region Operations methods

int addition(int a, int b)
{
	return a + b;
}

long addition(long a, long b)
{
	return a + b;
}

float addition(float a, float b)
{
	return a + b;
}

double addition(double a, double b)
{
	return a + b;
}

int subtraction(int a, int b)
{
	return a - b;
}

long subtraction(long a, long b)
{
	return a - b;
}

float subtraction(float a, float b)
{
	return a - b;
}

double subtraction(double a, double b)
{
	return a - b;
}

int multiplication(int a, int b)
{
	return a * b;
}

long multiplication(long a, long b)
{
	return a * b;
}

float multiplication(float a, float b)
{
	return a * b;
}

double multiplication(double a, double b)
{
	return a * b;
}

int division(int a, int b)
{
	return a / b;
}

long division(long a, long b)
{
	return a / b;
}

float division(float a, float b)
{
	return a / b;
}

double division(double a, double b)
{
	return a / b;
}

#pragma endregion

struct Test
{
	char operation_sign;
	string type_name;
	long long operations_per_second;
	Test(char operation_sign, string type_name, long long operations_per_second)
		: operation_sign(operation_sign), type_name(type_name), operations_per_second(operations_per_second)
	{}
};

const bool OUTPUT_PROCESSING_INFO = true;
chrono::time_point<chrono::system_clock> start_point, end_point;

const int a_int_default = 0, b_int_default = 1, c_int_default = 1;
int a_int, b_int, c_int;
const long a_long_default = 0, b_long_default = 1, c_long_default = 1;
long a_long, b_long, c_long;
const float a_float_default = 0, b_float_default = 1, c_float_default = 1;
float a_float, b_float, c_float;
const double a_double_default = 0, b_double_default = 1, c_double_default = 1;
double a_double, b_double, c_double;

char sign;
char addition_sign = '+';
char subtraction_sign = '-';
char multiplication_sign = '*';
char division_sign = '/';

string type_name;
string int_type_name = "integer";
string long_type_name = "long";
string float_type_name = "float";
string double_type_name = "double";

vector<Test> tests;

int interations_quantity = 1000 * 1000;
long long operations_per_second = 0;
chrono::duration<double> elapsed_seconds = chrono::duration<double>::max();
chrono::duration<double> loop_time = chrono::duration<double>::max();
time_t end_time;

chrono::duration<double> count_loop_time()
{
	chrono::time_point<chrono::system_clock> start_point, end_point;
	start_point = chrono::system_clock::now();
	for (int i = 0; i < interations_quantity; i++)
	{	}
	end_point = chrono::system_clock::now();
	return (end_point - start_point);
}


void output_processing_info()
{
	if (OUTPUT_PROCESSING_INFO)
	{
		if (&end_time != NULL)
			std::cout << "Computations finished at " << ctime(&end_time) << endl;
		std::cout << "Operations per second : " << setiosflags(ios::fixed) << setprecision(14) << operations_per_second << endl
			<< "Elapsed time : " << setiosflags(ios::fixed) << setprecision(10) << elapsed_seconds.count() << endl
			//<< "Loop time : " << setiosflags(ios::fixed) << setprecision(10) << loop_time.count()
			<< endl << endl;
	}
}

string get_percent_string(int percents)
{
	char symbol = '*';
	string out = "";
	int quantity = percents * 20 / 100;
	for (int i = 0; i < quantity; i++)
		out += symbol;
	return out;
}

void result_output()
{
	long long max_quantity = 0;
	for (auto const& value : tests)
	{
		if (value.operations_per_second > max_quantity)
			max_quantity = value.operations_per_second;
	}
	for (auto const& value : tests)
	{
		int percents = int(value.operations_per_second * 100 / max_quantity);
		std::cout << setw(5) << value.operation_sign << setw(12) << value.type_name << setw(20) << value.operations_per_second << setw(8) << percents << "%" << setw(24) << get_percent_string(percents) << endl;
	}
}

void reinitialize_variables()
{
	a_int = a_int_default;
	b_int = b_int_default;
	c_int = c_int_default;

	a_long = a_long_default;
	b_long = b_long_default;
	c_long = c_long_default;

	a_float = a_float_default;
	b_float = b_float_default;
	c_float = c_float_default;

	a_double = a_double_default;
	b_double = b_double_default;
	c_double = c_double_default;
}

int main()
{
	//initialize

	a_int = a_int_default;
	b_int = b_int_default;
	c_int = c_int_default;

	a_long = a_long_default;
	b_long = b_long_default;
	c_long = c_long_default;

	a_float = a_float_default;
	b_float = b_float_default;
	c_float = c_float_default;

	a_double = a_double_default;
	b_double = b_double_default;
	c_double = c_double_default;

	loop_time = count_loop_time();

#pragma region Integer

//----------------------------------------------------------------------------------------------------------------------------------------
// int

	if (OUTPUT_PROCESSING_INFO) cout << "Addition integer" << endl;
	sign = addition_sign;
	type_name = int_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_int = b_int + c_int;
		b_int = a_int + c_int;
		c_int = a_int + b_int;

		a_int = b_int + c_int;
		b_int = a_int + c_int;
		c_int = a_int + b_int;

		a_int = b_int + c_int;
		b_int = a_int + c_int;
		c_int = a_int + b_int;

		a_int = b_int + c_int;
		b_int = a_int + c_int;
		c_int = a_int + b_int;

		a_int = b_int + c_int;
		b_int = a_int + c_int;
		c_int = a_int + b_int;

		a_int = b_int + c_int;
		b_int = a_int + c_int;
		c_int = a_int + b_int;

		a_int = b_int + c_int;
		b_int = a_int + c_int;
		c_int = a_int + b_int;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	double a = elapsed_seconds.count();
	double b = loop_time.count();
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Subtraction integer" << endl;
	sign = subtraction_sign;
	type_name = int_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_int = b_int - c_int;
		b_int = a_int - c_int;
		c_int = a_int - b_int;

		a_int = b_int - c_int;
		b_int = a_int - c_int;
		c_int = a_int - b_int;

		a_int = b_int - c_int;
		b_int = a_int - c_int;
		c_int = a_int - b_int;

		a_int = b_int - c_int;
		b_int = a_int - c_int;
		c_int = a_int - b_int;

		a_int = b_int - c_int;
		b_int = a_int - c_int;
		c_int = a_int - b_int;

		a_int = b_int - c_int;
		b_int = a_int - c_int;
		c_int = a_int - b_int;

		a_int = b_int - c_int;
		b_int = a_int - c_int;
		c_int = a_int - b_int;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Multiplication integer" << endl;
	sign = multiplication_sign;
	type_name = int_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_int = b_int * c_int;
		b_int = a_int * c_int;
		c_int = a_int * b_int;

		a_int = b_int * c_int;
		b_int = a_int * c_int;
		c_int = a_int * b_int;

		a_int = b_int * c_int;
		b_int = a_int * c_int;
		c_int = a_int * b_int;

		a_int = b_int * c_int;
		b_int = a_int * c_int;
		c_int = a_int * b_int;

		a_int = b_int * c_int;
		b_int = a_int * c_int;
		c_int = a_int * b_int;

		a_int = b_int * c_int;
		b_int = a_int * c_int;
		c_int = a_int * b_int;

		a_int = b_int * c_int;
		b_int = a_int * c_int;
		c_int = a_int * b_int;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Division integer" << endl;
	sign = division_sign;
	type_name = int_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_int = b_int / c_int;
		b_int = a_int / c_int;
		c_int = a_int / b_int;

		a_int = b_int / c_int;
		b_int = a_int / c_int;
		c_int = a_int / b_int;

		a_int = b_int / c_int;
		b_int = a_int / c_int;
		c_int = a_int / b_int;

		a_int = b_int / c_int;
		b_int = a_int / c_int;
		c_int = a_int / b_int;

		a_int = b_int / c_int;
		b_int = a_int / c_int;
		c_int = a_int / b_int;

		a_int = b_int / c_int;
		b_int = a_int / c_int;
		c_int = a_int / b_int;

		a_int = b_int / c_int;
		b_int = a_int / c_int;
		c_int = a_int / b_int;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

#pragma endregion

#pragma region Long

//----------------------------------------------------------------------------------------------------------------------------------------
// long

	if (OUTPUT_PROCESSING_INFO) cout << "Addition long" << endl;
	sign = addition_sign;
	type_name = long_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_long = b_long + c_long;
		b_long = a_long + c_long;
		c_long = a_long + b_long;

		a_long = b_long + c_long;
		b_long = a_long + c_long;
		c_long = a_long + b_long;

		a_long = b_long + c_long;
		b_long = a_long + c_long;
		c_long = a_long + b_long;

		a_long = b_long + c_long;
		b_long = a_long + c_long;
		c_long = a_long + b_long;

		a_long = b_long + c_long;
		b_long = a_long + c_long;
		c_long = a_long + b_long;

		a_long = b_long + c_long;
		b_long = a_long + c_long;
		c_long = a_long + b_long;

		a_long = b_long + c_long;
		b_long = a_long + c_long;
		c_long = a_long + b_long;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Subtraction long" << endl;
	sign = subtraction_sign;
	type_name = long_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_long = b_long - c_long;
		b_long = a_long - c_long;
		c_long = a_long - b_long;

		a_long = b_long - c_long;
		b_long = a_long - c_long;
		c_long = a_long - b_long;

		a_long = b_long - c_long;
		b_long = a_long - c_long;
		c_long = a_long - b_long;

		a_long = b_long - c_long;
		b_long = a_long - c_long;
		c_long = a_long - b_long;

		a_long = b_long - c_long;
		b_long = a_long - c_long;
		c_long = a_long - b_long;

		a_long = b_long - c_long;
		b_long = a_long - c_long;
		c_long = a_long - b_long;

		a_long = b_long - c_long;
		b_long = a_long - c_long;
		c_long = a_long - b_long;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Multiplication long" << endl;
	sign = multiplication_sign;
	type_name = long_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_long = b_long * c_long;
		b_long = a_long * c_long;
		c_long = a_long * b_long;

		a_long = b_long * c_long;
		b_long = a_long * c_long;
		c_long = a_long * b_long;

		a_long = b_long * c_long;
		b_long = a_long * c_long;
		c_long = a_long * b_long;

		a_long = b_long * c_long;
		b_long = a_long * c_long;
		c_long = a_long * b_long;

		a_long = b_long * c_long;
		b_long = a_long * c_long;
		c_long = a_long * b_long;

		a_long = b_long * c_long;
		b_long = a_long * c_long;
		c_long = a_long * b_long;

		a_long = b_long * c_long;
		b_long = a_long * c_long;
		c_long = a_long * b_long;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Division long" << endl;
	sign = division_sign;
	type_name = long_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_long = b_long / c_long;
		b_long = a_long / c_long;
		c_long = a_long / b_long;

		a_long = b_long / c_long;
		b_long = a_long / c_long;
		c_long = a_long / b_long;

		a_long = b_long / c_long;
		b_long = a_long / c_long;
		c_long = a_long / b_long;

		a_long = b_long / c_long;
		b_long = a_long / c_long;
		c_long = a_long / b_long;

		a_long = b_long / c_long;
		b_long = a_long / c_long;
		c_long = a_long / b_long;

		a_long = b_long / c_long;
		b_long = a_long / c_long;
		c_long = a_long / b_long;

		a_long = b_long / c_long;
		b_long = a_long / c_long;
		c_long = a_long / b_long;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

#pragma endregion


#pragma region Float

//----------------------------------------------------------------------------------------------------------------------------------------
// float

	if (OUTPUT_PROCESSING_INFO) cout << "Addition float" << endl;
	sign = addition_sign;
	type_name = float_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_float = b_float + c_float;
		b_float = a_float + c_float;
		c_float = a_float + b_float;

		a_float = b_float + c_float;
		b_float = a_float + c_float;
		c_float = a_float + b_float;

		a_float = b_float + c_float;
		b_float = a_float + c_float;
		c_float = a_float + b_float;

		a_float = b_float + c_float;
		b_float = a_float + c_float;
		c_float = a_float + b_float;

		a_float = b_float + c_float;
		b_float = a_float + c_float;
		c_float = a_float + b_float;

		a_float = b_float + c_float;
		b_float = a_float + c_float;
		c_float = a_float + b_float;

		a_float = b_float + c_float;
		b_float = a_float + c_float;
		c_float = a_float + b_float;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Subtraction float" << endl;
	sign = subtraction_sign;
	type_name = float_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_float = b_float - c_float;
		b_float = a_float - c_float;
		c_float = a_float - b_float;

		a_float = b_float - c_float;
		b_float = a_float - c_float;
		c_float = a_float - b_float;

		a_float = b_float - c_float;
		b_float = a_float - c_float;
		c_float = a_float - b_float;

		a_float = b_float - c_float;
		b_float = a_float - c_float;
		c_float = a_float - b_float;

		a_float = b_float - c_float;
		b_float = a_float - c_float;
		c_float = a_float - b_float;

		a_float = b_float - c_float;
		b_float = a_float - c_float;
		c_float = a_float - b_float;

		a_float = b_float - c_float;
		b_float = a_float - c_float;
		c_float = a_float - b_float;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Multiplication float" << endl;
	sign = multiplication_sign;
	type_name = float_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_float = b_float * c_float;
		b_float = a_float * c_float;
		c_float = a_float * b_float;

		a_float = b_float * c_float;
		b_float = a_float * c_float;
		c_float = a_float * b_float;

		a_float = b_float * c_float;
		b_float = a_float * c_float;
		c_float = a_float * b_float;

		a_float = b_float * c_float;
		b_float = a_float * c_float;
		c_float = a_float * b_float;

		a_float = b_float * c_float;
		b_float = a_float * c_float;
		c_float = a_float * b_float;

		a_float = b_float * c_float;
		b_float = a_float * c_float;
		c_float = a_float * b_float;

		a_float = b_float * c_float;
		b_float = a_float * c_float;
		c_float = a_float * b_float;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Division float" << endl;
	sign = division_sign;
	type_name = float_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_float = b_float / c_float;
		b_float = a_float / c_float;
		c_float = a_float / b_float;

		a_float = b_float / c_float;
		b_float = a_float / c_float;
		c_float = a_float / b_float;

		a_float = b_float / c_float;
		b_float = a_float / c_float;
		c_float = a_float / b_float;

		a_float = b_float / c_float;
		b_float = a_float / c_float;
		c_float = a_float / b_float;

		a_float = b_float / c_float;
		b_float = a_float / c_float;
		c_float = a_float / b_float;

		a_float = b_float / c_float;
		b_float = a_float / c_float;
		c_float = a_float / b_float;

		a_float = b_float / c_float;
		b_float = a_float / c_float;
		c_float = a_float / b_float;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

#pragma endregion


#pragma region Double

//----------------------------------------------------------------------------------------------------------------------------------------
// double

	if (OUTPUT_PROCESSING_INFO) cout << "Addition double" << endl;
	sign = addition_sign;
	type_name = double_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_double = b_double + c_double;
		b_double = a_double + c_double;
		c_double = a_double + b_double;

		a_double = b_double + c_double;
		b_double = a_double + c_double;
		c_double = a_double + b_double;

		a_double = b_double + c_double;
		b_double = a_double + c_double;
		c_double = a_double + b_double;

		a_double = b_double + c_double;
		b_double = a_double + c_double;
		c_double = a_double + b_double;

		a_double = b_double + c_double;
		b_double = a_double + c_double;
		c_double = a_double + b_double;

		a_double = b_double + c_double;
		b_double = a_double + c_double;
		c_double = a_double + b_double;

		a_double = b_double + c_double;
		b_double = a_double + c_double;
		c_double = a_double + b_double;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Subtraction double" << endl;
	sign = subtraction_sign;
	type_name = double_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_double = b_double - c_double;
		b_double = a_double - c_double;
		c_double = a_double - b_double;

		a_double = b_double - c_double;
		b_double = a_double - c_double;
		c_double = a_double - b_double;

		a_double = b_double - c_double;
		b_double = a_double - c_double;
		c_double = a_double - b_double;

		a_double = b_double - c_double;
		b_double = a_double - c_double;
		c_double = a_double - b_double;

		a_double = b_double - c_double;
		b_double = a_double - c_double;
		c_double = a_double - b_double;

		a_double = b_double - c_double;
		b_double = a_double - c_double;
		c_double = a_double - b_double;

		a_double = b_double - c_double;
		b_double = a_double - c_double;
		c_double = a_double - b_double;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Multiplication double" << endl;
	sign = multiplication_sign;
	type_name = double_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_double = b_double * c_double;
		b_double = a_double * c_double;
		c_double = a_double * b_double;

		a_double = b_double * c_double;
		b_double = a_double * c_double;
		c_double = a_double * b_double;

		a_double = b_double * c_double;
		b_double = a_double * c_double;
		c_double = a_double * b_double;

		a_double = b_double * c_double;
		b_double = a_double * c_double;
		c_double = a_double * b_double;

		a_double = b_double * c_double;
		b_double = a_double * c_double;
		c_double = a_double * b_double;

		a_double = b_double * c_double;
		b_double = a_double * c_double;
		c_double = a_double * b_double;

		a_double = b_double * c_double;
		b_double = a_double * c_double;
		c_double = a_double * b_double;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

	if (OUTPUT_PROCESSING_INFO) cout << "Division double" << endl;
	sign = division_sign;
	type_name = double_type_name;
	start_point = chrono::system_clock::now();
	reinitialize_variables();
	for (int i = 0; i < interations_quantity; i++)
	{
		a_double = b_double / c_double;
		b_double = a_double / c_double;
		c_double = a_double / b_double;

		a_double = b_double / c_double;
		b_double = a_double / c_double;
		c_double = a_double / b_double;

		a_double = b_double / c_double;
		b_double = a_double / c_double;
		c_double = a_double / b_double;

		a_double = b_double / c_double;
		b_double = a_double / c_double;
		c_double = a_double / b_double;

		a_double = b_double / c_double;
		b_double = a_double / c_double;
		c_double = a_double / b_double;

		a_double = b_double / c_double;
		b_double = a_double / c_double;
		c_double = a_double / b_double;

		a_double = b_double / c_double;
		b_double = a_double / c_double;
		c_double = a_double / b_double;
	}
	end_point = chrono::system_clock::now();

	elapsed_seconds = end_point - start_point;
	end_time = chrono::system_clock::to_time_t(end_point);
	operations_per_second = 21.0 * double(interations_quantity) / double(elapsed_seconds.count() - loop_time.count());
	tests.push_back({ sign, type_name, operations_per_second });
	output_processing_info();

#pragma endregion


	result_output();
	getchar();
	return 0;
}

/*
without loop - 0.0000003990s
with loop (1mln) - 

*/